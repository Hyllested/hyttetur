package com.hyllested.helle.hyttetur.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.lang.reflect.Array;

/**
 * Created by helle on 24-03-2016.
 */
public class SqLiteHelper extends SQLiteOpenHelper {


    // Database
    public static final String DATABASE_NAME = "Hyttetur.db";
    public static final int DATABASE_VERSION = 4;

    // Users
    public static final String USERS_TABLE = "users";
    public static final String USERS_COLUMN_ID = "id";
    public static final String USERS_COLUMN_NAME = "name";


    public SqLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE " + USERS_TABLE + "(" + USERS_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + USERS_COLUMN_NAME + " TEXT)"
        );

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + USERS_TABLE);
        onCreate(db);
    }

    public void createUser(String userName) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL("INSERT INTO '" + USERS_TABLE + "'('" + USERS_COLUMN_NAME + "') VALUES ('" + userName + "')");
        } catch (SQLException e) {
            Log.d("database", e.toString());
        }
    }

    public Cursor getAllUsers() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + USERS_TABLE, null);
        return cursor;
    }
}
