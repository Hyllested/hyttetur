package com.hyllested.helle.hyttetur;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;


import com.hyllested.helle.hyttetur.database.SqLiteHelper;
import com.hyllested.helle.hyttetur.dummy.DummyContent;
import com.hyllested.helle.hyttetur.models.User;

import java.util.ArrayList;

/**
 * An activity representing a list of Personer. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link PersonDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class PersonListActivity extends AppCompatActivity {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    private SqLiteHelper sqLiteHelper;
    private ArrayList<User> userArrayList;

    private String[] drawerItems;
    private DrawerLayout drawerLayout;
    private ListView navListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        //TODO: move to resource
        drawerItems = new String[]{"Køb", "Oversigt", "Indstillinger"};
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        navListView = (ListView) findViewById(R.id.navListView);

        navListView.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, drawerItems));
        navListView.setOnItemClickListener(new DrawerItemClickListener());

        sqLiteHelper = new SqLiteHelper(this);

        View recyclerView = findViewById(R.id.person_list);
        assert recyclerView != null;
        setupRecyclerView((RecyclerView) recyclerView);

        /*sqLiteHelper.createUser("Henrik");*/

        if (findViewById(R.id.person_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {

        Cursor cursor = sqLiteHelper.getAllUsers();
        userArrayList = new ArrayList<User>();

        try {
            while (cursor.moveToNext()) {
                int id = cursor.getInt(0);
                String name = cursor.getString(1);
                userArrayList.add(new User(id, name));
            }
        } finally {
            cursor.close();
        }

        recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(userArrayList));
    }

    public class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final ArrayList<User> mValues;

        public SimpleItemRecyclerViewAdapter(ArrayList<User> userList) {
            mValues = userList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.person_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.mItem = mValues.get(position);
            holder.mNameView.setText(mValues.get(position).getName());


            holder.mNameView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mTwoPane) {
                        Bundle arguments = new Bundle();
                        arguments.putString(PersonDetailFragment.ARG_ITEM_ID, holder.mItem.getName());
                        PersonDetailFragment fragment = new PersonDetailFragment();
                        fragment.setArguments(arguments);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.person_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = v.getContext();
                        Intent intent = new Intent(context, PersonDetailActivity.class);
                        intent.putExtra(PersonDetailFragment.ARG_ITEM_ID, holder.mItem.getName());

                        context.startActivity(intent);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final TextView mNameView;
            /*public final TextView mContentView;*/
            public User mItem;

            public ViewHolder(View view) {
                super(view);
                mNameView = (TextView) view.findViewById(R.id.id);
            }

            @Override
            public String toString() {
                return super.toString();
            }
        }
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener{

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectNavItem(position);
        }
    }

    private void selectNavItem(int position) {
        Context context = this;
        Intent intent;
        switch(position) {
            case 0:
                intent = new Intent(context, PersonDetailActivity.class);
                break;
            case 1:
                intent = new Intent();
                break;
            case 2:
                intent = new Intent(context, SettingListActivity.class);
                break;
            default:
                intent = new Intent(context, PersonDetailActivity.class);
        }
        context.startActivity(intent);
    }
}
