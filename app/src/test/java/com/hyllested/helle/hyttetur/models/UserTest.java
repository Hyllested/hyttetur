package com.hyllested.helle.hyttetur.models;

import android.test.suitebuilder.annotation.SmallTest;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@SmallTest
public class UserTest{

    @Test
    public void user_constructor(){
        User user = new User(1 , "Helle");
        assertThat(user.getId(), is(equalTo(1)));
        assertThat(user.getName(), is(equalTo("Helle")));

    }
}